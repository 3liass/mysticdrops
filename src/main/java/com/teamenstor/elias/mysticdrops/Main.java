package com.teamenstor.elias.mysticdrops;

import com.teamenstor.elias.mysticdrops.commands.Command_MysticDrops;
import com.teamenstor.elias.mysticdrops.events.GUIListener;
import com.teamenstor.elias.mysticdrops.managers.ConfigManager;
import com.teamenstor.elias.mysticdrops.managers.GUIManager;
import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.SaveUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.DropBase;
import com.teamenstor.elias.mysticdrops.utility.interfaces.InventoryGUI;
import com.teamenstor.elias.mysticdrops.utility.interfaces.ItemDrop;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Main extends JavaPlugin {
	static {
		ConfigurationSerialization.registerClass(DropBase.class, "DropBase");
		ConfigurationSerialization.registerClass(ItemDrop.class, "ItemDrop");
	}

	public static String pluginName, pluginVersion;

	public static LinkedHashMap<ItemStack, DropBase> blockDrops;

	public static ConfigManager configManager;

	@Getter
	public static Logger pluginLogger;
	@Getter
	public static ConsoleCommandSender pluginConsoleSender;
	@Getter
	public static Main instance;

	@Override
	public void onEnable () {
		try {

			/* Initializing plugin information and classes */
			instance = this;
			pluginName = getDescription().getName();
			pluginVersion = getDescription().getVersion();
			pluginLogger = getLogger();
			pluginConsoleSender = getServer().getConsoleSender();

			configManager = new ConfigManager(this);

			/* Initializing configuration files */
			configManager.getConfig("config.yml").copyFromResource();
			configManager.getConfig("drops.yml").copyFromResource();

			/* Loading data from saved files */
			getLogger().log(Level.INFO, "Loading configuration...");
			if (SaveUtility.loadBlockDrops()) {
				getLogger().log(Level.INFO, "successfully loaded configuration from drops.yml.");
			} else {
				getLogger().log(Level.WARNING, "failed to load configuration from drops.yml.");
				blockDrops = new LinkedHashMap<>();
			}

			/* Registering GUIs */
			GUIManager guiManager = new GUIManager();

			/* Registering Commands, Events and Listeners */
			getServer().getPluginManager().registerEvents(new GUIListener(), this);

			Command_MysticDrops command_mysticDrops = new Command_MysticDrops();
			getCommand("mysticdrops").setExecutor(command_mysticDrops);
			getCommand("mysticdrops").setTabCompleter(command_mysticDrops);
			getCommand("mysticdrops").setPermissionMessage(MessageUtility.noPermission());

			/* Startup console logging */
			getLogger().log(Level.INFO, "was successfully enabled.");
		} catch (Exception exception) {
			getLogger().log(Level.SEVERE, "failed on startup.");
			MessageUtility.logException(exception, getClass());
			this.setEnabled(false);
		}
	}

	@Override
	public void onDisable () {
		try {
			if ((boolean) configManager.getConfig("config.yml").get("autosave_ondisable") && blockDrops.size() > 0) {
				getLogger().log(Level.INFO, "Saving configuration...");
				if (SaveUtility.saveBlockDrops()) {
					getLogger().log(Level.INFO, "successfully saved configuration to drops.yml.");
				} else {
					getLogger().log(Level.WARNING, "failed to save configuration to drops.yml.");
					blockDrops = new LinkedHashMap<>();
				}
			}

			/* Close all the open inventories when plugin is disabled (for reloads) */
			InventoryGUI.getOpenInventories().forEach((playerUuid, inventoryUuid) -> {
				Bukkit.getPlayer(playerUuid).closeInventory();
			});

			/* Shutdown console logging */
			getLogger().log(Level.INFO, "was successfully disabled.");
		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
		}
	}

}
