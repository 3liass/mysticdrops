package com.teamenstor.elias.mysticdrops.utility.interfaces;

import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class InventoryGUI {

	/* Inventory UUID, Inventory Instance */
	@Getter
	private static Map<UUID, InventoryGUI> inventoriesByUUID = new HashMap<>();
	/* Player UUID, Inventory UUID; to store which players have which inventory open */
	@Getter
	private static Map<UUID, UUID> openInventories = new HashMap<>();

	@Getter
	private UUID uuid;
	@Getter
	private Inventory inventory;
	@Getter
	private Map<Integer, GUIAction> actions;
	@Getter
	private int size;
	@Getter
	private GUIType type;

	public InventoryGUI (int size, String name, GUIType type) {
		uuid = UUID.randomUUID();
		this.size = size;
		this.type = type;
		inventory = Bukkit.createInventory(null, size, name);
		actions = new HashMap<>();
		inventoriesByUUID.put(getUuid(), this);
	}

	/* Methods for the inventory */
	protected void setItem (int slot, ItemStack item, GUIAction action) {
		try {
			inventory.setItem(slot, item);
			if (action != null) actions.put(slot, action);
		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
		}
	}

	protected void setItem (int slot, ItemStack item) {
		setItem(slot, item, null);
	}

	protected void setItems (int[] slots, ItemStack item, GUIAction action) {
		try {
			for (int slot : slots) {
				setItem(slot, item, action);
			}
		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
		}
	}

	public void setItems (int[] slots, ItemStack item) {
		setItems(slots, item, null);
	}

	public void open (Player player) {
		try {
			player.openInventory(inventory);
			openInventories.put(player.getUniqueId(), getUuid());
		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
		}
	}

	public void updateItems () {

	}

	public void closeExecutions (Player player) {

	}

	protected void closeAll () {
		try {
			openInventories.forEach((key, value) -> {
				if (value.equals(getUuid()))
					Bukkit.getPlayer(key).closeInventory();
			});
		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
		}
	}

	protected void delete () {
		try {
			inventoriesByUUID.remove(getUuid());
		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
		}
	}

	protected ItemStack getItem (int slot) {
		try {
			return getInventory().getItem(slot);
		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
			return null;
		}
	}

	/* Interface for actions (clicks) */
	public interface GUIAction {
		void click (Player player, InventoryClickEvent clickEvent);
	}

	/* Enum for gui type */
	public enum GUIType {
		EDITABLE,
		UNEDITABLE
	}

}
