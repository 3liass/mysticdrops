package com.teamenstor.elias.mysticdrops.utility.interfaces;

import com.google.common.io.ByteStreams;
import com.teamenstor.elias.mysticdrops.Main;
import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.nio.file.Paths;

public class Config {

	private String name;
	private File file;
	private YamlConfiguration config;
	private Main plugin;

	public Config (Main plugin, String name) {
		this.plugin = plugin;
		this.name = name;
		file = Paths.get(plugin.getDataFolder().toString(), name).toFile();
		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
			} catch (IOException exception) { MessageUtility.logException(exception, getClass()); }
		}
	}

	public Config save () {
		if ((this.config == null) || (this.file == null)) { return this; }
		try {
			if (config.getConfigurationSection("").getKeys(true).size() != 0) {
				config.save(this.file);
			}
		} catch (IOException exception) { MessageUtility.logException(exception, getClass()); }
		return this;
	}

	public YamlConfiguration get () {
		if (this.config == null) { reload(); }
		return this.config;
	}

	public Config saveDefauktConfig () {
		file = new File(plugin.getDataFolder(), this.name);
		plugin.saveResource(this.name, false);
		return this;
	}

	public Config clear () throws FileNotFoundException {
		new PrintWriter(file).close();
		return this;
	}

	public Config reload () {
		if (file == null) {
			this.file = new File(plugin.getDataFolder(), this.name);
		}
		this.config = YamlConfiguration.loadConfiguration(file);
		Reader defConfigStream;

		try {
			defConfigStream = new InputStreamReader(plugin.getResource(this.name), "UTF8");

			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
			this.config.setDefaults(defConfig);
		} catch (UnsupportedEncodingException | NullPointerException exception) { MessageUtility.logException(exception, getClass()); }
		return this;
	}

	public Config copyDefaults (boolean force) {
		get().options().copyDefaults(force);
		return this;
	}

	public Config copyFromResource () {
		if (file.length() <= 0) {
			try {
				try (InputStream in = plugin.getResource(this.file.getName());
					 OutputStream out = new FileOutputStream(this.file)) {
					ByteStreams.copy(in, out);
				}
			} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
			try { config = YamlConfiguration.loadConfiguration(this.file); }
			catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
		}
		return this;
	}

	public Config copyFromResource (String folder) {
		if (file.length() <= 0) {
			try {
				try (InputStream in = plugin.getResource(folder + "/" + this.file.getName());
					 OutputStream out = new FileOutputStream(this.file)) {
					ByteStreams.copy(in, out);
				}
			} catch (Exception exception) {
				MessageUtility.logException(exception, getClass());
			}
			try {
				config = YamlConfiguration.loadConfiguration(this.file);
			} catch (Exception exception) {
				MessageUtility.logException(exception, getClass());
			}
		}
		return this;
	}

	public Config set (String key, Object value) {
		get().set(key, value);
		return this;
	}

	public Object get (String key) { return get().get(key); }

}
