package com.teamenstor.elias.mysticdrops.utility;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SpawnEggMeta;

import java.util.List;

public class ItemUtility {

	/* Create item with name and lore using ItemStack */
	public static ItemStack createItem (ItemStack item, String name, List<String> lore) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}

	/* Create item with name and lore using Material */
	public static ItemStack createItem (Material item, String name, List<String> lore) { return createItem(new ItemStack(item), name, lore); }

	/* Create item with name using ItemStack */
	public static ItemStack createItem (ItemStack item, String name) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		return item;
	}

	/* Create item with name using Material */
	public static ItemStack createItem (Material item, String name) {
		return createItem(new ItemStack(item), name);
	}

	/* Create a spawn egg */
	public static ItemStack createSpawnEgg (EntityType type, String name) {
		ItemStack item = new ItemStack(Material.MONSTER_EGG, 1);
		SpawnEggMeta meta = (SpawnEggMeta) item.getItemMeta();
		meta.setSpawnedType(type);
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		return item;
	}

	public static String getItemName(Material material){
		return WordUtils.capitalizeFully(material.name().replace("_", " "));
	}

}
