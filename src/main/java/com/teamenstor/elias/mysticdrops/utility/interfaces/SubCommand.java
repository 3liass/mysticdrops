package com.teamenstor.elias.mysticdrops.utility.interfaces;

import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.command.CommandSender;

public class SubCommand extends CommandBase {

	@Getter
	private CommandBase base;

	public SubCommand (CommandBase base, String name, String permission, String description) {
		super(name, permission, description);
		this.base = base;
	}

	@Override
	public void sendCommandHelp (CommandSender sender) {
		try {
			ComponentBuilder builder = new ComponentBuilder(ChatColor.DARK_AQUA + "------- MysticDrops - Help -------");
			String commandName = "/" + getBase().getName() + " " + getName() + " ";
			String coloredCommandName = ChatColor.DARK_GRAY + "/" + ChatColor.AQUA + getBase().getName() + " " + getName() + " ";
			MessageUtility.appendLine(commandName.substring(0, commandName.length() - (getName().length() + 1)), coloredCommandName.substring(0, coloredCommandName.length() - (getName().length() + 1)), builder, getName(), getDescription(), getPermission());
			if (getArguments() != null)
				getArguments().forEach((key, sub) -> {
					if (sub != null)
						MessageUtility.appendLine(commandName, getName(), coloredCommandName, builder, key, sub.getDescription(), sub.getPermission());
				});
			builder.append("\n" + ChatColor.DARK_AQUA + "-------------------------------").reset();
			sender.spigot().sendMessage(builder.create());
		}catch(Exception exception){
			MessageUtility.logException(exception, getClass());
		}
	}
}
