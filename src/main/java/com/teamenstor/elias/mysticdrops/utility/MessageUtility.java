package com.teamenstor.elias.mysticdrops.utility;

import com.teamenstor.elias.mysticdrops.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.time.Year;
import java.util.Arrays;

public class MessageUtility {

	public static String noPermission () {
		return ChatColor.translateAlternateColorCodes('&', (String) Main.configManager.getConfig("config.yml").get("no_permission"));
	}

	public static void sendPluginInfo (CommandSender sender) {
		try {
			ComponentBuilder builder = new ComponentBuilder(ChatColor.DARK_AQUA + "------- MysticDrops - Info -------");
			builder.append("\n" + ChatColor.AQUA + "Modify block and mob drops!");
			builder.append("\n" + ChatColor.AQUA + "Developed by Dillen.");
			builder.append("\n" + ChatColor.AQUA + "Copyright MysticMC - " + Year.now());
			builder.append("\n" + ChatColor.DARK_AQUA + "-------------------------------").reset();
			sender.spigot().sendMessage(builder.create());
		} catch (Exception e) { logException(e, MessageUtility.class); }
	}

	public static void sendPluginInfo (Player player) {
		sendPluginInfo((CommandSender) player);
	}

	public static void appendLine (String command, String coloredCommand, ComponentBuilder builder, String argument, String description, String permission) {
		try {
			String fullCommand = command + argument;
			builder.append("\n")
				.append(coloredCommand + argument)
				.event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, fullCommand))
				.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText(argument, description, fullCommand, permission)));
		} catch (Exception e) { logException(e, MessageUtility.class); }
	}

	public static void appendLine (String command, String subCommand, String coloredCommand, ComponentBuilder builder, String argument, String description, String permission) {
		try {
			String fullCommand = command + argument;
			builder.append("\n")
				.append(coloredCommand + argument)
				.event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, fullCommand))
				.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText(subCommand, description, fullCommand, permission)));
		} catch (Exception e) { logException(e, MessageUtility.class); }
	}

	private static BaseComponent[] hoverText (String command, String description, String usage, String permission) {
		try {
			ComponentBuilder compBuilder = new ComponentBuilder
				(ChatColor.DARK_AQUA + "Command: " + ChatColor.AQUA + command)
				.append("\n" + ChatColor.DARK_AQUA + "Description: " + ChatColor.AQUA + description)
				.append("\n" + ChatColor.DARK_AQUA + "Usage: " + ChatColor.AQUA + usage)
				.append("\n" + ChatColor.DARK_AQUA + "Permission: " + ChatColor.AQUA + permission)
				.append("\n\n" + ChatColor.DARK_GRAY + "Click to auto-complete");
			return compBuilder.create();
		} catch (Exception e) { logException(e, MessageUtility.class); }
		return null;
	}

	public static void logException (Exception exception, Class thrownClass) {
		boolean debug;
		try {
			debug = (boolean) Main.configManager.getConfig("config.yml").get("debug");
		} catch (Exception e) {
			debug = true;
		}
		if (debug) {
			exception.printStackTrace();
		} else {
			Arrays.stream(exception.getStackTrace()).filter(exc -> {
				assert exc.getFileName() != null;
				return exc.getFileName().toLowerCase().contains(thrownClass.getSimpleName().toLowerCase());
			}).forEach(exc -> Main.getPluginConsoleSender().sendMessage("[" + Main.pluginName + "] " + org.bukkit.ChatColor.RED + String.format("Exception in %s on line %s caused by: %s", thrownClass.getSimpleName(), exc.getLineNumber(), exception.getMessage())));
		}
	}

	public static String getItemName (ItemStack item) {
		try {
			net.minecraft.server.v1_12_R1.ItemStack nmsStack = org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack.asNMSCopy(item);
			return nmsStack.getName();
		} catch (Exception e) { logException(e, MessageUtility.class); }
		return null;
	}

}
