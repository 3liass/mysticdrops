package com.teamenstor.elias.mysticdrops.utility.interfaces;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SerializableAs("ItemDrop")
public class ItemDrop implements Cloneable, ConfigurationSerializable {

	/* Which item should be dropped when activated */
	@Getter
	private ItemStack item;

	/* Chance of activation */
	@Getter
	private Double chance;

	/* If the plugin shall cancel the original drops upon activation */
	@Getter
	private Boolean cancel_default;

	/* In which worlds it can be activated */
	@Getter
	private List<World> worlds;

	/* Disable the possibility of activation */
	@Getter
	private Boolean enabled;

	/* If the plugin shall send a message to the player upon activation */
	@Getter
	private Boolean send_message;

	/* What message the plugin shall send to the player upon activation */
	@Getter
	private String message;

	public ItemDrop () {
		Initialize(new ItemStack(Material.STONE, 1), 0d, false, new ArrayList<>(), false, false, "&4Lucky!! You got a special drop!");
	}

	public ItemDrop (ItemStack item, double chance, boolean cancel_default, List<World> worlds, boolean enabled, boolean send_message, String message) {
		Initialize(item, chance, cancel_default, worlds, enabled, send_message, message);
	}

	private void Initialize (ItemStack item, double chance, boolean cancel_default, List<World> worlds, boolean enabled, boolean send_message, String message) {
		this.item = item;
		this.chance = chance;
		this.cancel_default = cancel_default;
		this.worlds = worlds;
		this.enabled = enabled;
		this.send_message = send_message;
		this.message = message;
	}

	/* Serialization methods */
	@Override
	public Map<String, Object> serialize () {
		LinkedHashMap<String, Object> serialize = new LinkedHashMap<>();
		serialize.put("item", item);
		serialize.put("chance", chance);
		serialize.put("cancel_default", cancel_default);
		serialize.put("worlds", worlds);
		serialize.put("enabled", enabled);
		serialize.put("send_message", send_message);
		serialize.put("message", message);
		return serialize;
	}

	public static ItemDrop deserialize (Map<String, Object> args) {
		ItemStack item = (args.containsKey("item") ? (ItemStack) args.get("item") : new ItemStack(Material.STONE, 1));
		double chance = (args.containsKey("chance") ? (Double) args.get("chance") : 0d);
		boolean cancel_default = (args.containsKey("cancel_default") ? (Boolean) args.get("cancel_default") : false);
		List<World> worlds = (args.containsKey("worlds") ? (List<World>) args.get("worlds") : new ArrayList<>());
		boolean enabled = (args.containsKey("enabled") ? (Boolean) args.get("enabled") : false);
		boolean send_message = (args.containsKey("send_message") ? (Boolean) args.get("send_message") : false);
		String message = (args.containsKey("message") ? (String) args.get("message") : "&4Lucky!! You got a special drop!");

		return new ItemDrop(item, chance, cancel_default, worlds, enabled, send_message, message);
	}
}