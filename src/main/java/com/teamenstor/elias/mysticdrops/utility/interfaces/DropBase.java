package com.teamenstor.elias.mysticdrops.utility.interfaces;

import lombok.Getter;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SerializableAs ( "DropBase" )
public class DropBase implements Cloneable, ConfigurationSerializable {

	/* Disable the possibility of activation */
	@Getter
	private Boolean enabled;

	/* List of ItemDrops */
	@Getter
	List<ItemDrop> drops;

	public DropBase (boolean enabled, List<ItemDrop> drops) {
		Initialize(enabled, drops);
	}

	public DropBase () {
		Initialize(false, new ArrayList<ItemDrop>(){{ add(new ItemDrop()); }});
	}

	private void Initialize (boolean enabled, List<ItemDrop> drops) {
		this.enabled = enabled;
		this.drops = drops;
	}

	/* Serialization methods */
	@Override
	public Map<String, Object> serialize () {
		LinkedHashMap<String, Object> serialize = new LinkedHashMap<>();
		serialize.put("enabled", enabled);
		serialize.put("drops", drops);
		return serialize;
	}

	public static DropBase deserialize (Map<String, Object> args) {
		boolean enabled = (args.containsKey("enabled") ? (Boolean) args.get("enabled") : false);
		List<ItemDrop> drops = (args.containsKey("drops") ? (List<ItemDrop>) args.get("drops") : new ArrayList<ItemDrop>(){{ add(new ItemDrop()); }});
		return new DropBase(enabled, drops);
	}
}
