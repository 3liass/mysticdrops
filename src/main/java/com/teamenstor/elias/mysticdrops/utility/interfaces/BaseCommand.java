package com.teamenstor.elias.mysticdrops.utility.interfaces;

import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.command.CommandSender;

import java.util.*;
import java.util.stream.Collectors;

public class BaseCommand extends CommandBase {

	public BaseCommand (String name, String permission, String description) {
		super(name, permission, description);
	}

	@Override
	public void sendCommandHelp (CommandSender sender) {
		try {
			ComponentBuilder builder = new ComponentBuilder(ChatColor.DARK_AQUA + "------- MysticDrops - Help -------");

			String commandName = "/" + getName() + " ";
			String coloredCommandName = ChatColor.DARK_GRAY + "/" + ChatColor.AQUA + getName() + " ";

			getArguments().forEach((key, sub) -> {
				if (sub != null)
					MessageUtility.appendLine(commandName, coloredCommandName, builder, key, sub.getDescription(), sub.getPermission());
			});
			builder.append("\n" + ChatColor.DARK_AQUA + "-------------------------------").reset();
			sender.spigot().sendMessage(builder.create());
		}catch(Exception exception){
			MessageUtility.logException(exception, getClass());
		}
	}

	protected List<String> getTabCompletions (CommandSender sender, String[] args) {
		try {
			List<String> output = new ArrayList<>();
			int position = 0;

			List<String> filteredArgs = Arrays.stream(args).filter(String -> !String.isEmpty()).collect(Collectors.toList());
			filteredArgs.replaceAll(String::toLowerCase);

			if (args.length == 1) {
				getArguments().forEach((key, value) -> {
					if (sender.hasPermission(value.getPermission()))
						output.add(key);
				});
			} else if (args.length > 1) {
				if (filteredArgs.size() <= 0)
					return output;
				SubCommand sub = getArguments().get(filteredArgs.get(0));
				for (int i = 1; i < filteredArgs.size(); i++) {
					position = i;
					if (sub != null && sub.getArguments() != null) {
						SubCommand potentialSub = sub.getArguments().get(filteredArgs.get(i));
						if (potentialSub != null) {
							position++;
							sub = potentialSub;
						} else break;
					} else break;
				}
				position = (position == 0 ? 1 : position);
				if (sub != null && sender.hasPermission(sub.getPermission())) {
					sub.updateTabCompletions();
					if (!(args.length > position + 1)) {
						if (!sub.getTabCompletions().isEmpty())
							output.addAll(sub.getTabCompletions());
						else if (sub.getArguments() != null)
							output.addAll(sub.getArguments().keySet());
					}
				}
			}
			final int finalPosition = position;
			if (finalPosition >= args.length)
				return new ArrayList<String>() {{
					add(args[finalPosition - 1]);
				}};
			return output.stream().filter(s -> s.toLowerCase().startsWith(args[finalPosition])).collect(Collectors.toList());
		}catch(Exception exception){
			MessageUtility.logException(exception, getClass());
			return null;
		}
	}
	protected void updateSubTabCompletion (){
		getArguments().values().forEach(CommandBase::updateTabCompletions);
	}
}
