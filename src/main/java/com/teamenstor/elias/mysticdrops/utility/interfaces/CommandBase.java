package com.teamenstor.elias.mysticdrops.utility.interfaces;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class CommandBase {

	@Getter
	private String name;
	@Getter
	private String permission;
	@Getter
	private String description;
	@Setter
	@Getter
	private LinkedHashMap<String, SubCommand> arguments;
	@Getter
	private List<String> tabCompletions;
	@Getter
	private CommandBase instance;

	public CommandBase (String name, String permission, String description) {
		instance = this;
		this.name = name;
		this.permission = permission;
		this.description = description;
		this.arguments = new LinkedHashMap<>();
		tabCompletions = new ArrayList<>();
	}

	public void updateTabCompletions () { }

	public boolean execute (CommandSender sender, org.bukkit.command.Command command, String label, String[] args) { return false; }

	public void sendCommandHelp (CommandSender sender) { }

	public void sendCommandHelp (Player player) {
		sendCommandHelp((CommandSender) player);
	}

}
