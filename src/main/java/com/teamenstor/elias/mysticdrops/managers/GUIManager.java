package com.teamenstor.elias.mysticdrops.managers;

import com.teamenstor.elias.mysticdrops.guis.blocks.GUI_AddBlock;
import com.teamenstor.elias.mysticdrops.guis.blocks.GUI_BlocksMain;
import com.teamenstor.elias.mysticdrops.guis.GUI_MainMenu;
import com.teamenstor.elias.mysticdrops.guis.blocks.GUI_Confirm;
import lombok.Getter;

public class GUIManager {

	@Getter
	private static GUI_MainMenu mainMenu;

	public GUIManager () {
		mainMenu = new GUI_MainMenu();
	}

	public static GUI_BlocksMain getBlocksMain () {
		return new GUI_BlocksMain();
	}
	public static GUI_AddBlock getAddBlock() { return new GUI_AddBlock(); }
	public static GUI_Confirm getConfirm() { return new GUI_Confirm(); }

}
