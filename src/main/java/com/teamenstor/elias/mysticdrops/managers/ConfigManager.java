package com.teamenstor.elias.mysticdrops.managers;

import com.teamenstor.elias.mysticdrops.Main;
import com.teamenstor.elias.mysticdrops.utility.interfaces.Config;

import java.util.HashMap;

public class ConfigManager {

	private final Main plugin;
	private HashMap<String, Config> configs = new HashMap<>();

	public ConfigManager (Main plugin) {
		this.plugin = plugin;
	}

	public Config getConfig (String name) {
		if (!configs.containsKey((name))) { configs.put(name, new Config(plugin, name)); }
		return configs.get(name);
	}
}
