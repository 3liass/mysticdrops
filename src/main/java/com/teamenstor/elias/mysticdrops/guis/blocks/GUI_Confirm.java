package com.teamenstor.elias.mysticdrops.guis.blocks;

import com.teamenstor.elias.mysticdrops.Main;
import com.teamenstor.elias.mysticdrops.managers.GUIManager;
import com.teamenstor.elias.mysticdrops.utility.ItemUtility;
import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.DropBase;
import com.teamenstor.elias.mysticdrops.utility.interfaces.InventoryGUI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class GUI_Confirm extends InventoryGUI {

	private int state = 0;

	public GUI_Confirm () {
		super(54, ChatColor.DARK_AQUA + "MysticDrops - Confirm", GUIType.UNEDITABLE);
	}

	GUI_Confirm deleteQuery (ItemStack rawItem) {
		try {
			getInventory().clear();
			ItemStack item = rawItem.clone();

			ItemStack andesite = ItemUtility.createItem(new ItemStack(Material.STONE, 1, (short) 5), " ");
			int[] slots = { 13, 21, 23, 31 };
			setItems(slots, andesite, ((player, clickEvent) -> {
				clickEvent.setCancelled(true);
			}));

			setItem(22, ItemUtility.createItem(item.clone(), ChatColor.RED + "Are you sure you want to delete " + ChatColor.DARK_RED + ChatColor.stripColor(MessageUtility.getItemName(item.clone())) + ChatColor.RED + "?"));

			setItem(48, ItemUtility.createItem(Material.REDSTONE_BLOCK, ChatColor.RED + "Cancel"), (player, clickEvent) -> {
				GUIManager.getBlocksMain().open(player);
			});

			setItem(50, ItemUtility.createItem(Material.EMERALD_BLOCK, ChatColor.GREEN + "Yes"), (player, clickEvent) -> {
				Main.blockDrops.remove(rawItem.clone());
				endScreen(item, "deleted").open(player);
			});

			ItemStack extra = ItemUtility.createItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14), " ");
			for (int i = 0; i < getSize(); i++) {
				if (getItem(i) == null) {
					setItem(i, extra);
				}
			}
		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
		}
		return this;
	}

	GUI_Confirm addQuery (ItemStack rawItem) {
		try {
			getInventory().clear();
			ItemStack item = rawItem.clone();

			ItemStack andesite = ItemUtility.createItem(new ItemStack(Material.STONE, 1, (short) 5), " ");
			int[] slots = { 13, 21, 23, 31 };
			setItems(slots, andesite, ((player, clickEvent) -> {
				clickEvent.setCancelled(true);
			}));

			setItem(22, ItemUtility.createItem(item.clone(), ChatColor.RED + "Are you sure you want to add " + ChatColor.DARK_RED + ChatColor.stripColor(MessageUtility.getItemName(item.clone())) + ChatColor.RED + "?"));

			setItem(48, ItemUtility.createItem(Material.REDSTONE_BLOCK, ChatColor.RED + "Cancel"), (player, clickEvent) -> {
				GUIManager.getBlocksMain().open(player);
			});

			setItem(50, ItemUtility.createItem(Material.EMERALD_BLOCK, ChatColor.GREEN + "Yes"), (player, clickEvent) -> {
				Main.blockDrops.put(item, new DropBase());
				endScreen(item, "added").open(player);
			});

			ItemStack extra = ItemUtility.createItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14), " ");
			for (int i = 0; i < getSize(); i++) {
				if (getItem(i) == null) {
					setItem(i, extra);
				}
			}

		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
		}
		return this;
	}

	private GUI_Confirm endScreen (ItemStack item, String message) {
		try {
			getInventory().clear();
			setItem(22, ItemUtility.createItem(item.clone(), ChatColor.GREEN + "Successfully " + message + " " + ChatColor.DARK_GREEN + ChatColor.stripColor(MessageUtility.getItemName(item.clone())) + ChatColor.GREEN + "."));

			setItem(49, ItemUtility.createItem(Material.ARROW, ChatColor.RED + "Back"), ((player, clickEvent) -> {
				GUIManager.getBlocksMain().open(player);
			}));

			ItemStack extra = ItemUtility.createItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5), " ");
			for (int i = 0; i < getSize(); i++) {
				if (getItem(i) == null) {
					setItem(i, extra);
				}
			}

		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
		}
		return this;
	}

}
