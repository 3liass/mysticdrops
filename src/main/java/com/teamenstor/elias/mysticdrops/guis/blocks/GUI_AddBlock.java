package com.teamenstor.elias.mysticdrops.guis.blocks;

import com.teamenstor.elias.mysticdrops.Main;
import com.teamenstor.elias.mysticdrops.managers.GUIManager;
import com.teamenstor.elias.mysticdrops.utility.ItemUtility;
import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.InventoryGUI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class GUI_AddBlock extends InventoryGUI {

	/*
	 * States:
	 * 0 = default, add item
	 * 1 = error, item is not a block
	 * 2 = error, item is already in list
	 */
	private int state = 0;
	private ItemStack item;
	private Player player;

	public GUI_AddBlock () {
		super(54, ChatColor.DARK_AQUA + "MysticDrops - Add Block", GUIType.EDITABLE);
	}

	@Override
	public void updateItems () {
		try {
			if (state == 0) {
				getInventory().clear();
				ItemStack extra = ItemUtility.createItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), " ");
				for (int i = 0; i < getSize(); i++) {
					if (getItem(i) == null) {
						setItem(i, extra, ((player, clickEvent) -> {
							clickEvent.setCancelled(true);
						}));
					}
				}

				ItemStack andesite = ItemUtility.createItem(new ItemStack(Material.STONE, 1, (short) 5), " ");
				int[] slots = { 13, 21, 23, 31 };
				setItems(slots, andesite, ((player, clickEvent) -> {
					clickEvent.setCancelled(true);
				}));


				setItem(22, null, ((player, clickEvent) -> {
					ItemStack item = (getItem(22) == null ? clickEvent.getCursor() : getItem(22));
					if (item != null && item.getType() != Material.AIR) {
						clickEvent.setCancelled(true);
						clickEvent.getView().setCursor(null);
						this.item = item;
						player.getInventory().addItem(item);
						item.setItemMeta(new ItemStack(item.getType()).getItemMeta());
						item.setAmount(1);
						this.player = player;
						if (!item.getType().isBlock()) {
							state = 1;
							updateItems();
						} else if (Main.blockDrops.containsKey(item)) {
							state = 2;
							updateItems();
						} else {
							GUIManager.getConfirm().addQuery(item.clone()).open(player);
						}
					}
				}));
			} else if (state == 1 || state == 2) {
				getInventory().clear();
				ItemStack extra = ItemUtility.createItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14), " ");
				for (int i = 0; i < getSize(); i++) {
					if (getItem(i) == null) {
						setItem(i, extra, ((player, clickEvent) -> {
							clickEvent.setCancelled(true);
						}));
					}
				}

				ItemStack redstone_block = ItemUtility.createItem(new ItemStack(Material.REDSTONE_BLOCK, 1), " ");
				int[] slots = { 13, 21, 23, 31 };
				setItems(slots, redstone_block, ((player, clickEvent) -> {
					clickEvent.setCancelled(true);
				}));


				setItem(22, ItemUtility.createItem(item,
					ChatColor.RED + MessageUtility.getItemName(item) + (state == 1 ? " is not a block." : " already exists."),
					Arrays.asList(ChatColor.GRAY + "Item must be a block.", ChatColor.GRAY + "Click to try again.")),
					((player, clickEvent) -> {
						clickEvent.setCancelled(true);
						state = 0;
						updateItems();
					}));
			}

			setItem(49, ItemUtility.createItem(Material.ARROW, ChatColor.RED + "Back"), ((player, clickEvent) -> {
				GUIManager.getBlocksMain().open(player);
			}));
		} catch (Exception exception) {
			MessageUtility.logException(exception, getClass());
		}
	}

	@Override
	public void open (Player player) {
		updateItems();
		super.open(player);
	}

	@Override
	public void closeExecutions (Player player) {
		delete();
	}

}
