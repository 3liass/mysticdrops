package com.teamenstor.elias.mysticdrops.commands.subcommands;

import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.CommandBase;
import com.teamenstor.elias.mysticdrops.utility.interfaces.SubCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class SubCommand_Help extends SubCommand {

	public SubCommand_Help (CommandBase base) {
		super(base, "help", "mysticdrops.help", "Displays a list of commands.");
		getArguments().put("[sub-command]", new SubCommand(base, "[sub-command]", "mysticdrops.help", "Displays a list of commands from a sub-command"));
	}

	@Override
	public void updateTabCompletions () {
		try {
			getTabCompletions().clear();
			getTabCompletions().addAll(getBase().getArguments().keySet());
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
	}

	@Override
	public boolean execute (CommandSender sender, Command command, String label, String[] args) {
		try {
			if (!sender.hasPermission(getPermission())) {
				sender.sendMessage(MessageUtility.noPermission());
			} else if (args.length == 0) { getBase().sendCommandHelp(sender); } else if (args.length == 1) {
				SubCommand sub = getBase().getArguments().get(args[0].toLowerCase());
				if (sub == null) { sendCommandHelp(sender); } else sub.sendCommandHelp(sender);
			} else { sendCommandHelp(sender); }
			return true;
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
		return false;
	}
}
