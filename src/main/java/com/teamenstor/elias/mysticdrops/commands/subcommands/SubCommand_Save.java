package com.teamenstor.elias.mysticdrops.commands.subcommands;

import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.SaveUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.CommandBase;
import com.teamenstor.elias.mysticdrops.utility.interfaces.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class SubCommand_Save extends SubCommand {

	public SubCommand_Save (CommandBase base) {
		super(base, "save", "mysticdrops.drops.save", "Save configuration to file.");
	}

	@Override
	public boolean execute (CommandSender sender, Command command, String label, String[] args) {
		try {
			if (!sender.hasPermission(getPermission())) { sender.sendMessage(MessageUtility.noPermission()); }
			else if (args.length > 0) { sendCommandHelp(sender); } else {
				sender.sendMessage(ChatColor.GREEN + "Saving configuration to drops.yml file...");
				if (SaveUtility.saveBlockDrops()) sender.sendMessage(ChatColor.GREEN + "Configuration saved.");
				else sender.sendMessage(ChatColor.RED + "Failed to save configuration.");
			}
			return true;
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
		return false;
	}
}
