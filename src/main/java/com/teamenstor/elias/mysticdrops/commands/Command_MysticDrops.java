package com.teamenstor.elias.mysticdrops.commands;

import com.teamenstor.elias.mysticdrops.Main;
import com.teamenstor.elias.mysticdrops.commands.subcommands.*;
import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.BaseCommand;
import com.teamenstor.elias.mysticdrops.utility.interfaces.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class Command_MysticDrops extends BaseCommand implements CommandExecutor, TabCompleter {

	public Command_MysticDrops () {
		super("mysticdrops", "mysticdrops.use", "Main command for MysticDrops.");
		getArguments().put("info", new SubCommand_Info(this));
		getArguments().put("drops", new SubCommand_Drops(this));
		getArguments().put("reload", new SubCommand_Reload(this));
		getArguments().put("help", new SubCommand_Help(this));
		updateSubTabCompletion();
	}

	@Override
	public boolean onCommand (CommandSender sender, Command command, String label, String[] args) {
		try {
			if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "Only players are able to execute this command.");
				return true;
			}
			if (args.length == 0) {
				if ((boolean) Main.configManager.getConfig("config.yml").get("send_help")) { sendCommandHelp(sender); }
				else { MessageUtility.sendPluginInfo(sender); }
				return true;
			}
			SubCommand subCommand = getArguments().get(args[0]);
			if (subCommand != null) {
				return subCommand.execute(sender, command, label, Arrays.copyOfRange(args, 1, args.length));
			} else {
				sendCommandHelp(sender);
				return true;
			}
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
		return false;
	}

	@Override
	public List<String> onTabComplete (CommandSender sender, Command command, String alias, String[] args) { return getTabCompletions(sender, args); }
}
