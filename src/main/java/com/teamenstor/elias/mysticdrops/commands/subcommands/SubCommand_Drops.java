package com.teamenstor.elias.mysticdrops.commands.subcommands;

import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.CommandBase;
import com.teamenstor.elias.mysticdrops.utility.interfaces.SubCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

public class SubCommand_Drops extends SubCommand {


	public SubCommand_Drops (CommandBase base) {
		super(base, "drops", "mysticdrops.drops", "Manage drops.");
		getArguments().put("edit", new SubCommand_Edit(base));
		getArguments().put("save", new SubCommand_Save(base));
		getArguments().put("revert", new SubCommand_Revert(base));
		getArguments().put("help", new SubCommand_DropsHelp(this));
	}

	@Override
	public boolean execute (CommandSender sender, Command command, String label, String[] args) {
		try {
			if (args.length == 0) { sendCommandHelp(sender); }
			else {
				SubCommand subCommand = getArguments().get(args[0]);
				if (subCommand != null) {
					return subCommand.execute(sender, command, label, Arrays.copyOfRange(args, 1, args.length));
				} else { sendCommandHelp(sender); }
			}
			return true;
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
		return false;
	}
}
