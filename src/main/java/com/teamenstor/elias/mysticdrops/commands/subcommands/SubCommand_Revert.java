package com.teamenstor.elias.mysticdrops.commands.subcommands;

import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.SaveUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.CommandBase;
import com.teamenstor.elias.mysticdrops.utility.interfaces.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class SubCommand_Revert extends SubCommand {

	public SubCommand_Revert (CommandBase base) {
		super(base, "revert", "mysticdrops.drops.revert", "Revert configuration to last saved state in drops.yml file.");
	}

	@Override
	public boolean execute (CommandSender sender, Command command, String label, String[] args) {
		try {
			if (!sender.hasPermission(getPermission())) {
				sender.sendMessage(MessageUtility.noPermission());
				return true;
			}
			if (args.length > 0) {
				sendCommandHelp(sender);
				return true;
			}
			sender.sendMessage(ChatColor.GREEN + "Reverting configuration...");
			if (SaveUtility.loadBlockDrops()) sender.sendMessage(ChatColor.GREEN + "Reverted the configuration.");
			else sender.sendMessage(ChatColor.RED + "Failed to revert the configuration.");
			return true;
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
		return false;
	}
}
